#!/usr/bin/env sh

. ./common/functions.sh

selectFromList "copy symlink" "Create fake binaries as: "
action="${REPLY}"

selectFromList "yes no" "Create files without extension ?"
extensionLess="${REPLY}"

getUserInput "Set folder where files will be created" "/usr/local/bin"
outputFolder="${REPLY}"

actionCommand=""
if [ "copy" = "${action}" ]; then
  actionCommand="cp"
elif [ "symlink" = "${action}" ]; then
  actionCommand="ln -s"
fi

errors=""
if [ ! -d "${outputFolder}" ]; then
  errors="${errors}\n- target folder does not exist"
fi
if [ -z "${actionCommand}" ]; then
  errors="${errors}\n- invalid actionCommand/action"
fi

if [ -n "${errors}" ]; then
  echo "Error(s) occurred: ${errors}"
  exit 1
fi

baseDir="$(realpath "$(dirname "${0}")")"
for file in bin-scripts/*; do
  createFakeBinaryFile "${baseDir}" "${file}" "${outputFolder}" "${actionCommand}" "${extensionLess}"
done

exit 0