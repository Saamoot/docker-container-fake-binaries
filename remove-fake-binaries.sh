#!/usr/bin/env sh

. ./common/functions.sh

selectFromList "yes no" "Created files are without extension ?"
extensionLess="${REPLY}"

getUserInput "Set folder where files are located" "/usr/local/bin"
outputFolder="${REPLY}"

asSudo
SUDO="${REPLY}"

for file in bin-scripts/*; do
  createOutputFile "${file}" "${outputFolder}" "${extensionLess}"
  outputFile="${REPLY}"

  $SUDO rm -f "${outputFile}"
done