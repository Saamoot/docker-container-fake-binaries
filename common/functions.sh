#!/usr/bin/env sh

selectFromList() {
  list="${1}"
  message="${2:-}"

  if [ -z "${message}" ]; then
    message="Options:"
  fi

  count="$(echo "${list}" | wc -w)"
  echo ""

  i=1
  echo "${message}"
  for option in $list; do
    echo "${i}) ${option}"
    i=$((i + 1))
  done

  n=""
  while true; do
    echo
    printf "Select option: "
    read -r n

    # If $n is an integer between one and $count...
    if [ "$n" -eq "$n" ] && [ "$n" -gt 0 ] && [ "$n" -le "${count}" ]; then
      break
    fi

    echo "Please set value between: <1,${count}>"
  done

  option="$(echo "${list}" | cut -d' ' -f"${n}")"
  echo "You have selected \"${option}\""

  REPLY="${option}"
}


getUserInput() {
  message="${1}"
  fallbackValue="${2:-}"

  if [ -n "${fallbackValue}" ]; then
    message="${message} [${fallbackValue}]"
  fi

  echo ""
  echo "${message}: "
  read -r userInput

  if [ -z "${userInput}" ]; then
    userInput="${fallbackValue}"
  fi

  REPLY="${userInput}"
}

asSudo() {
  REPLY=""
  if [ "root" != "$(whoami)" ]; then
    REPLY="sudo"
  fi
}

createOutputFile() {
  file="${1}"
  outputFolder="${2}"
  extensionLess="${3}"

  outputFile="$(basename "${file}")"
  if [ "yes" = "${extensionLess}" ]; then
    outputFile="${outputFile%.*}"
  fi

  REPLY="${outputFolder}/${outputFile}"
}

createFakeBinaryFile() {
    baseDir="${1}"
    file="${2}"
    outputFolder="${3}"
    createCommand="${4}"
    extensionLess="${5}"

    asSudo
    SUDO="${REPLY}"

    sourceFile="${baseDir}/${file}"
    createOutputFile "${file}" "${outputFolder}" "${extensionLess}"
    outputFile="${outputFolder}/${outputFile}"

    if [ -f "${outputFile}" ]; then
      echo "Skipping \"$(basename "${file}")\", file: \"${outputFile}\" already exist."
      return
    fi

    echo "Creating \"${outputFile}\""
    eval "${SUDO} ${createCommand} ${sourceFile} ${outputFile}"
    $SUDO chmod u+x "${outputFile}"
}