#!/bin/sh

#
#Dockerhub: https://hub.docker.com/_/composer/
#

#In case of problem with ssh:
#    --volume /etc/passwd:/etc/passwd:ro \
#    --volume /etc/group:/etc/group:ro \

COMPOSER_HOME="${COMPOSER_HOME:-"${HOME}/.composer"}"
COMPOSER_CACHE_DIR="${COMPOSER_CACHE_DIR:-"${HOME}/.cache/composer"}"

docker run \
  --tty \
  --interactive \
  --rm \
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  --volume "${COMPOSER_HOME}":"${COMPOSER_HOME}" \
  --volume "${COMPOSER_CACHE_DIR}":"${COMPOSER_CACHE_DIR}" \
  --volume "${SSH_AUTH_SOCK}":"${SSH_AUTH_SOCK}" \
  --env SSH_AUTH_SOCK="${SSH_AUTH_SOCK}" \
  --env COMPOSER_HOME="${COMPOSER_HOME}" \
  --env COMPOSER_CACHE_DIR="${COMPOSER_CACHE_DIR}" \
  --user "$(id -u)":"$(id -g)" \
  composer:latest \
  "$@"
