#!/usr/bin/env sh

#
# https://hub.docker.com/r/bitnami/kubectl/
#

volumes=""
volumes="${volumes} --volume ${HOME}/.kube:/.kube"

minikubeDir="${HOME}/.minikube"
if [ -d "${minikubeDir}" ]; then
  volumes="${volumes} --volume ${minikubeDir}:${minikubeDir}"
fi

dynamicMounts=""
for arg in ${@}; do
  if [ "${arg#*"-"}" != "${arg}" ]; then
    # skip shfmt argument
    continue
  fi

  arg="$(realpath "${arg}")"

  if [ -f "${arg}" ]; then
    dynamicMounts="${dynamicMounts} --volume ${arg}:${arg}"
  fi
done

docker run \
  --rm \
  ${volumes} \
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  ${dynamicMounts} \
  --user "$(id -u)":"$(id -g)" \
  --network host \
  bitnami/kubectl:latest \
  "${@}"
