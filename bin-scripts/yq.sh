#!/usr/bin/env sh

docker run\
  --rm\
  --interactive\
  --tty\
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  --user "$(id -u)":"$(id -g)"\
  mikefarah/yq\
  "$@"
