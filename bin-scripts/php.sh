#!/bin/sh

#
#Dockerhub: https://hub.docker.com/_/php
#

DOCKER_IMAGE_PHP_VERSION="${DOCKER_IMAGE_PHP_VERSION:-"latest"}"

dynamicMounts=""
for arg in ${@}; do
  if [ "${arg#*"-"}" != "${arg}" ]; then
    # skip shfmt argument
    continue
  fi

  arg="$(realpath "${arg}")"

  if [ -f "${arg}" ]; then
    dynamicMounts="${dynamicMounts} --volume ${arg}:${arg}"
  fi
done

docker run \
  --tty \
  --interactive \
  --rm \
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  ${dynamicMounts} \
  --user "$(id -u)":"$(id -g)" \
  --entrypoint=/usr/local/bin/php \
  php:${DOCKER_IMAGE_PHP_VERSION} \
  "$@"
