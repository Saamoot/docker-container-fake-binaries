#!/bin/sh

#
#Dockerhub: https://hub.docker.com/r/alpine/helm
#

volumes=""
volumes="${volumes} --volume ${HOME}/.kube:/root/.kube"
volumes="${volumes} --volume ${HOME}/.helm:/root/.helm"
volumes="${volumes} --volume ${HOME}/.config/helm:/root/.config/helm"
volumes="${volumes} --volume ${HOME}/.cache/helm:/root/.cache/helm"

minikubeDir="${HOME}/.minikube"
if [ -d "${minikubeDir}" ]; then
  volumes="${volumes} --volume ${minikubeDir}:${minikubeDir}"
fi

dynamicMounts=""
for arg in ${@}; do
  if [ "${arg#*"-"}" != "${arg}" ]; then
    # skip shfmt argument
    continue
  fi

  arg="$(realpath "${arg}")"

  if [ -f "${arg}" ]; then
    dynamicMounts="${dynamicMounts} --volume ${arg}:${arg}"
  fi
done

docker run \
  --tty \
  --interactive \
  --rm \
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  ${dynamicMounts} \
  --user "$(id -u)":"$(id -g)" \
  ${volumes} \
  --network host \
  alpine/helm:latest \
  "$@"
