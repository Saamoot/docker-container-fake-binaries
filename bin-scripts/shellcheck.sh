#!/bin/bash

dynamicMounts=""
for arg in ${@}; do
  if [ "${arg#*"-"}" != "${arg}" ]; then
    # skip shfmt argument
    continue
  fi

  arg="$(realpath "${arg}")"

  if [ -f "${arg}" ]; then
    dynamicMounts="${dynamicMounts} --volume ${arg}:${arg}"
  fi
done


docker run \
  --rm \
	--volume "$(pwd)":"$(pwd)" \
	--workdir "$(pwd)"  \
  ${dynamicMounts} \
	--user "$(id -u)":"$(id -g)" \
  koalaman/shellcheck:latest \
  "$@"
