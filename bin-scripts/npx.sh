#!/bin/bash

docker run\
  --rm\
  --interactive\
  --tty\
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  --user "$(id -u)":"$(id -g)"\
  node:current-buster-slim\
  bash -c "npx $*"
