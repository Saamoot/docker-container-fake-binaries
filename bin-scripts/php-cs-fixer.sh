#!/usr/bin/env sh

dynamicMounts=""
for arg in ${@}; do
  if [ "${arg#*"-"}" != "${arg}" ]; then
    # skip shfmt argument
    continue
  fi

  arg="$(realpath "${arg}")"

  if [ -f "${arg}" ]; then
    dynamicMounts="${dynamicMounts} --volume ${arg}:${arg}"
  fi
done

docker run --rm \
  --user "$(id -u)":"$(id -g)"\
  --volume "$(pwd)":"$(pwd)" \
  --workdir "$(pwd)" \
  ${dynamicMounts} \
  acceleratedzerodevelopment/php-cs-fixer \
  "${@}"
